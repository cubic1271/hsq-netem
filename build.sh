#!/usr/bin/env bash

if [ ! -f lib/dpdk/include/rte_ring.h ]; then
	echo "Please ensure lib/dpdk exists and is a symlink to the *build output* directory of dpdk"
	exit -1
fi

mkdir ./build 2> /dev/null
pushd ./build

if [ $? -ne 0 ]; then
	echo "Unable to cd into 'build' directory"
	exit -1
fi

cmake ..
if [ $? -ne 0 ]; then
	echo "CMake failed to run ..."
	exit -1
fi

make
if [ $? -ne 0 ]; then
	echo "Code failed to build ..."
	exit -1
fi

