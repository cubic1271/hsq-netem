#include "core/common.h"
#include "core/io_worker.h"

static int lcore_hello(__attribute__((unused)) void *arg)
{
    unsigned lcore_id;
    lcore_id = rte_lcore_id();
    printf("hello from core %u\n", lcore_id);
    return 0;
}

int main(int argc, char **argv)
{
    int ret;
    unsigned int lcore_id;
    int io_worker_lcore = -1;

    ret = rte_eal_init(argc, argv);
    if (ret < 0) {
        rte_panic("EAL initialization failed ...\n");
    }

    lcore_id = rte_get_master_lcore();

    RTE_LCORE_FOREACH_SLAVE(lcore_id) {
        if(io_worker_lcore == -1) {
            io_worker_lcore = lcore_id;
            rte_eal_remote_launch(hsq_thr_io_worker, NULL, lcore_id);
            continue;
        }
        // rte_eal_remote_launch(lcore_hello, NULL, lcore_id);
    }

    /* call it on master lcore too */
    lcore_hello(NULL);

    rte_eal_mp_wait_lcore();
    return 0;
}
