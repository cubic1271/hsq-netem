#include "core/common.h"
#include "core/object.h"

static int hsq_thr_io_worker(__attribute__((unused)) void *arg)
{
    unsigned int lcore_id;
    lcore_id = rte_lcore_id();
    rte_log(RTE_LOG_INFO, RTE_LOGTYPE_USER2, "Initializing I/O manager on core %u\n", lcore_id);
    return 0;
}
