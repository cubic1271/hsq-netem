﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using CommNet;
using KSPLink.worker;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace KSPLink
{
    [KSPAddon(KSPAddon.Startup.AllGameScenes, false)]
    public class KSPBridgeAdapter : MonoBehaviour
    {
        private const float HEARTBEAT_INTERVAL = 5.0f;

        private static readonly int messagePort = 7191;
        private static bool shouldInit = true;

        private static float lastUpdateTime = 0.0f;
        private static float lastWriteTime = 0.0f;
        private static Socket messageSocket;
        private static Socket incomingSocket;
        
        /*
         * See: http://docs.unity3d.com/Documentation/ScriptReference/MonoBehaviour.Awake.html,
         */
        public KSPBridgeAdapter()
        {
            Debug.Log("[KSPBridge] Instantiating KSPBridgeAdapter ...");
        }

        /*
         * Called after the scene is loaded.
         */
        void Awake()
        {
            if (shouldInit)
            {
                Debug.Log("[KSPBridge] KSPBridgeAdapter is starting up.");
                incomingSocket = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);
                incomingSocket.Blocking = false;
                incomingSocket.Bind(new IPEndPoint(IPAddress.Any, messagePort));
                incomingSocket.Listen(1);
                shouldInit = false;
            }
        }

        /*
         * Called next.
         */
        void Start()
        {

        }

        /*
         * Called every frame
         */
        void Update()
        {
            lastUpdateTime = Time.unscaledTime;
            
            if (messageSocket == null)
            {
                try
                {
                    messageSocket = incomingSocket.Accept();
                    messageSocket.Blocking = false;
                    Debug.Log("[KSPBridge] New message connection established: " + ((IPEndPoint)messageSocket.RemoteEndPoint).ToString());
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode != SocketError.WouldBlock)
                    {
                        throw ex;
                    }
                }
            }

            if (messageSocket != null && messageSocket.Available > 0)
            {
                handleMessage();
            }

            if (lastUpdateTime - lastWriteTime > HEARTBEAT_INTERVAL) /* Write once per minute ... */
            {
                lastWriteTime = lastUpdateTime;
                if (messageSocket != null)
                {
                    try
                    {
                        AppMessage.write(AppMessageType.HEARTBEAT, messageSocket);
                    }
                    catch (IOException ex)
                    {
                        Debug.Log("[KSPBridge] Unable to send heartbeat to network worker: " + ex.Message);
                        Debug.Log("[KSPBridge] Shutting down connection ...");
                        messageSocket.Close();
                        messageSocket = null;
                    }
                    catch (SocketException ex)
                    {
                        Debug.Log("[KSPBridge] Unable to send heartbeat to network worker: " + ex.Message);
                        Debug.Log("[KSPBridge] Shutting down connection ...");
                        messageSocket.Close();
                        messageSocket = null;
                    }
                }
            }
        }

        /*
         * Called at a fixed time interval determined by the physics time step.
         */
        void FixedUpdate()
        {

        }

        /*
         * Called when the game is leaving the scene (or exiting). Perform any clean up work here.
         */
        void OnDestroy()
        {

        }

        void OnApplicationQuit()
        {
            Debug.Log("[KSPBridge] Sending shutdown notification ...");
            AppMessage.write(AppMessageType.SHUTDOWN, messageSocket);
        }

        void handleMessage()
        {
            AppMessage message = AppMessage.read(messageSocket);
            switch (message.type)
            {
                case AppMessageType.UPDATE_REQUEST:
                    handleUpdateRequest();
                    break;
                default:
                    break;
            }

        }

        int handleUpdateRequest()
        {
            // long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            Stopwatch watch = Stopwatch.StartNew();
            StringBuilder csv = new StringBuilder();
            
            float currTime = Time.unscaledTime;
            for (int i = 0; i < FlightGlobals.Vessels.Count; ++i)
            {
                Vessel vessel = FlightGlobals.Vessels[i];
                if (vessel.DiscoveryInfo.HaveKnowledgeAbout(DiscoveryLevels.StateVectors))
                {
                    csv.AppendFormat("{0:0.0000},{1},\"{2}\",{3},{4},{5},{6:0.0000},vessel\n", 
                        currTime, vessel.GetInstanceID(), vessel.GetName(), 
                        vessel.transform.localPosition.x, vessel.transform.localPosition.y, vessel.transform.localPosition.z,
                        vessel.altitude);
                }
            }

            for (int i = 0; i < FlightGlobals.Bodies.Count; ++i)
            {
                CelestialBody body = FlightGlobals.Bodies[i];
                csv.AppendFormat("{0:0.0000},{1},\"{2}\",{3},{4},{5},{6:0.0000},body\n",
                    currTime, body.GetInstanceID(), body.GetName(),
                    body.transform.localPosition.x, body.transform.localPosition.y, body.transform.localPosition.z,
                    body.Radius);
            }

            byte[] tmpOut = Encoding.ASCII.GetBytes(csv.ToString());
            int res = AppMessage.write(AppMessageType.UPDATE_REPLY, tmpOut, messageSocket);
            watch.Stop();
            // milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            // Debug.Log(String.Format("[KSPBridge] Request fulfilled in {0:0.000}s  (@{1})", watch.ElapsedMilliseconds / 1000.0, milliseconds));
            return res;
        }
    }
}
