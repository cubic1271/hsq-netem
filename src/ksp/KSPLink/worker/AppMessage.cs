﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Debug = UnityEngine.Debug;

namespace KSPLink.worker
{
    enum AppMessageType
    {
        SHUTDOWN = 1,
        HEARTBEAT = 2,
        UPDATE_REQUEST = 3,
        UPDATE_REPLY = 4
    }

    class AppMessage
    {
        private const int APPMESSAGE_MAX_SIZE = 10000000;
        private const int APPMESSAGE_BUFFER_SIZE = 16384;
        private const int APPMESSAGE_MIN_SIZE = 5;

        public byte[]           inbuffer;
        public int              size;
        public AppMessageType   type;
        public byte[]           data;

        public static AppMessage read(Socket inSocket)
        {
            byte[] tbuffer = new byte[APPMESSAGE_BUFFER_SIZE];
            AppMessage message = new AppMessage();
            message.inbuffer = new byte[APPMESSAGE_MIN_SIZE];
            int rcount = 0;
            while (rcount < APPMESSAGE_MIN_SIZE)
            {
                try
                {
                    int res = inSocket.Receive(tbuffer);
                    for (int i = 0; i < res; ++i)
                    {
                        message.inbuffer[rcount + i] = tbuffer[i];
                    }
                    rcount += res;
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode != SocketError.WouldBlock)
                    {
                        Debug.Log("[AppMessage] Unable to read message ... ");
                        Debug.Log(ex);
                        throw;
                    }
                }
            }
            message.type = (AppMessageType)message.inbuffer[0];
            switch (message.type)
            {
                case AppMessageType.HEARTBEAT:
                    break;
                case AppMessageType.SHUTDOWN:
                    break;
                case AppMessageType.UPDATE_REQUEST:
                    break;
                case AppMessageType.UPDATE_REPLY:
                    break;
                default:
                    throw new IOException("read invalid type (" + message.type + ")");
            }
            message.size = (message.inbuffer[4]) | (message.inbuffer[3] << 8) | (message.inbuffer[2] << 16) |
                           (message.inbuffer[1] << 24);
            Debug.Log("Read message: " + message.type + " / " + message.size + " bytes");

            if (message.size > APPMESSAGE_MAX_SIZE)
            {
                throw new IOException("readSize of " + message.size + " exceeds maximum expected message size");
            }
            
            if (message.size - APPMESSAGE_MIN_SIZE > 0)
            {
                message.data = new byte[message.size - APPMESSAGE_MIN_SIZE];
                rcount = 0;
                while (rcount < message.size - APPMESSAGE_MIN_SIZE)
                {
                    try
                    {
                        int res = inSocket.Receive(tbuffer);
                        for (int i = 0; i < res; ++i)
                        {
                            message.data[rcount + i] = tbuffer[i];
                        }
                        rcount += res;
                    }
                    catch (SocketException ex)
                    {
                        if (ex.SocketErrorCode != SocketError.WouldBlock)
                        {
                            Debug.Log("[AppMessage] Unable to read message ... ");
                            Debug.Log(ex);
                            throw;
                        }
                    }
                }
            }
            else
            {
                message.data = null;
            }

            return message;
        }

        public static void write(AppMessageType type, Socket outSocket)
        {

            byte[] outBuffer = new byte[] { (byte)type, 0, 0, 0, APPMESSAGE_MIN_SIZE };
            bool completed = false;
            while (!completed)
            {
                try
                {
                    outSocket.Send(outBuffer);
                    completed = true;
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode != SocketError.WouldBlock)
                    {
                        throw;
                    }
                }

            }
        }

        public static int write(AppMessageType type, byte[] bytes, Socket outSocket)
        {
            bool completed = false;
            int writeSize = bytes.Length + APPMESSAGE_MIN_SIZE;
            byte[] outBuffer = new byte[] { (byte)type, 0, 0, 0, 0 };
            // convert to big-endian
            outBuffer[4] = (byte)((writeSize & 0x000000ff));
            outBuffer[3] = (byte)((writeSize & 0x0000ff00) >> 8);
            outBuffer[2] = (byte)((writeSize & 0x00ff0000) >> 16);
            outBuffer[1] = (byte)((writeSize & 0xff000000) >> 24);
            while (!completed)
            {
                try
                {
                    outSocket.Send(outBuffer);
                    completed = true;
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode != SocketError.WouldBlock)
                    {
                        throw;
                    }
                }

            }

            completed = false;
            while (!completed)
            {
                try
                {
                    outSocket.Send(bytes);
                    completed = true;
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode != SocketError.WouldBlock)
                    {
                        throw;
                    }
                }

            }

            return writeSize;
        }
    }
}
