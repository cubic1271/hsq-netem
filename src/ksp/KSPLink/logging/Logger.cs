﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UnityEngine.Assertions;

namespace KSPLink.Log
{
    enum KspBridgeLogLevel
    {
        TRACE = 2,
        DEBUG = 3,
        INFO = 4,
        WARN = 5,
        ERROR = 6,
        FATAL = 7,
        ALWAYS = 8
    }

    class KSPBridgeLogger
    {
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private string path;
        private System.IO.StreamWriter file;
        private bool mustInit = true;
        private KspBridgeLogLevel cutoff = KspBridgeLogLevel.INFO;

        public KSPBridgeLogger()
        {
            
        }

        public void level(KspBridgeLogLevel target)
        {
            if (target != KspBridgeLogLevel.ALWAYS)
            {
                this.cutoff = target;
            }
        }

        public void init(string path, bool append)
        {
            if (!mustInit)
            {
                log(KspBridgeLogLevel.ALWAYS, "Closing logfile ...");
                file.Close();
            }

            this.path = path;
            file = new System.IO.StreamWriter(this.path, append);
            mustInit = false;
            log(KspBridgeLogLevel.ALWAYS, "Opening logfile ...");
        }

        public void log(KspBridgeLogLevel level, string target)
        {
            Debug.Assert(!mustInit);
            if (level < cutoff)
            {
                return;
            }
            long ms = (long) ((DateTime.UtcNow - epoch).TotalMilliseconds);
            double ts = ms/1000.0;
            file.WriteLine("[" + ts + "] " + level + " | " + target);
            file.Flush();
        }

    }
}
