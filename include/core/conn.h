#ifndef HSQ_NETEM_CONN_H
#define HSQ_NETEM_CONN_H

#include "core/object.h"

typedef enum hsq_port_type_t {
    HSQ_PORT_NULL,
    HSQ_PORT_VIRTUAL,
    HSQ_PORT_REAL
} hsq_port_type_t;

typedef struct hsq_port_entry_t {
    int64_t id;
} hsq_port_entry_t;

typedef struct hsq_port_manager_t {
    uint32_t ports;

} hsq_port_t;

#endif //HSQ_NETEM_CONN_H
