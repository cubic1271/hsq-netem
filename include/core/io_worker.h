#ifndef HSQ_NETEM_IO_WORKER_H
#define HSQ_NETEM_IO_WORKER_H

/**
 * Launches the simulation thread used to keep track of current object position based on ingest from an external source,
 * and also handles exposing statistics and the like ...
 *
 * @return 0 on success
 */
static int hsq_thr_io_worker(__attribute__((unused)) void *arg);

typedef struct hsq_io_stats_t {
    uint64_t ingest_count;
    uint64_t ingest_bytes;
    uint64_t forward_count;
    uint64_t forward_bytes;
    uint32_t lcore;
    uint32_t _pad;
} hsq_io_stats_t;

typedef struct hsq_ingest_header_t {

} hsq_ingest_header_t;

#endif //HSQ_NETEM_IO_WORKER_H
