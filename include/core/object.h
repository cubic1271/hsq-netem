
#ifndef HSQ_NETEM_OBJECT_H
#define HSQ_NETEM_OBJECT_H

#include <stdint.h>

#define SPEED_OF_LIGHT (299792458)

/**
 * A representation of the position of an object in space
 */
typedef struct hsq_pos_t {
    double x;
    double y;
    double z;
} __attribute__((packed)) hsq_pos_t;


/**
 * A generic queue structure with a length and a pointer to a list o' stuff.
 */
typedef struct hsq_queue_t {
    uint64_t  length;
    rte_mbuf* packets;
};

/**
 * A core object used to track a position and a unique identification number.  The position is used for propagation
 * delay and distance / bandwidth calculations.
 */
typedef struct hsq_object_t {
    uint64_t  updated;
    uint32_t  id;
    hsq_pos_t position;
    uint64_t  _pad;
} __attribute__((packed)) hsq_object_t;

/**
 *  A structure which stores
 */
typedef struct hsq_header_t {
    uint64_t   timestamp;
    hsq_pos_t  origin;
} hsq_header_t;

/**
 * Calculates the distance between two objects
 *
 * @param first First object
 * @param second Second object
 * @return Distance between the two objects
 */
double hsq_distance(const hsq_pos_t first, const hsq_pos_t second);

#endif //HSQ_NETEM_OBJECT_H
